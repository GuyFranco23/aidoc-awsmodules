resource "aws_s3_bucket" "bucket" {
  bucket = "${var.bucket}"
  acl    = "private"

  tags {
    Name        = "${var.bucket}"
    Environment = "${var.env}"
    Owner = "${var.bucket}"
  }
}