###################33 webserver 
module "public-sg" {
    source = "terraform-aws-modules/security-group/aws"
  
    name = "${var.VPCName}-public-sg"
    vpc_id = "${module.vpc.vpc_id}"
    description = "Security group for public subnet"

    ingress_with_cidr_blocks = [
        
        {
            rule = "http-80-tcp"
            cidr_blocks = "0.0.0.0/0"
        },
        {
            rule = "ssh-tcp"
            cidr_blocks = "31.154.174.14/32"
        },
        {
             rule = "https-443-tcp"
            cidr_blocks = "31.154.174.14/32"
        },
  ]
   
   

    ingress_with_self = [{
    rule = "rdp-tcp"
    description = "RDP"
    },]


    egress_with_cidr_blocks = [{
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "0.0.0.0/0"
  }]

  
  }
##########################3 analysis
 module "Analysis-sg" {
         source = "terraform-aws-modules/security-group/aws"

    name = "${var.VPCName}-Analysis"
    vpc_id = "${module.vpc.vpc_id}"

    computed_ingress_with_source_security_group_id = [{
      rule = "http-80-tcp"
      source_security_group_id = "${module.public-sg.this_security_group_id}"
    },
    {
      rule = "https-443-tcp"
      source_security_group_id = "${module.public-sg.this_security_group_id}"
    }
    ]
//    number_of_computed_ingress_with_source_security_group_id = 2


  ingress_with_cidr_blocks = [{
        rule = "ssh-tcp"
        cidr_blocks = "31.154.174.14/32"
        description = "Aidoc TLV office SSH"
    },]
 
  egress_with_cidr_blocks = [{
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "0.0.0.0/0"
  },]
  
}
###################### load balance 
module "load-balancer-SG" {
   source = "terraform-aws-modules/security-group/aws"
  name = "${var.VPCName}-load-balancer-SG"
  vpc_id = "${module.vpc.vpc_id}"

  tags {
    Name = "${var.VPCName}-load-balancer-SG"
  }
ingress_with_cidr_blocks = [
        
        {
            rule = "http-80-tcp"
            cidr_blocks = "0.0.0.0/0"
        },
        
        {
             rule = "https-443-tcp"
            cidr_blocks = "31.154.174.14/32"
        },
  ]
  
 

  egress_with_cidr_blocks = [{
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "0.0.0.0/0"
  },]
}

################################3 rds


module "rds-sg" {
     source = "terraform-aws-modules/security-group/aws"

  name = "${var.VPCName}-rds-sg"
  vpc_id = "${module.vpc.vpc_id}"

  tags {
    Name = "${var.VPCName}-rds-sg"
  }

  ingress_with_cidr_blocks = [{
        rule = "postgresql-tcp"
        cidr_blocks = "31.154.174.14/32"
        description = "Aidoc TLV office tcp 5432 "
    },]

  computed_ingress_with_source_security_group_id = [{
      rule = "postgresql-tcp"
      source_security_group_id = "${module.Analysis-sg.this_security_group_id}"
    },
    {
      rule = "postgresql-tcp"
      source_security_group_id = "${module.public-sg.this_security_group_id}"
    }
    ]
  
   egress_with_cidr_blocks = [{
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "0.0.0.0/0"
  },]
}
