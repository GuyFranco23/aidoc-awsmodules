module "queue" {
  source = "terraform-aws-modules/sqs/aws"

  name = "${var.VPCName}-${var.queue_name}"

  tags = {
    Service     = "user"
    Environment = "dev"
  }
  delay_seconds             = 0
    max_message_size          = 262144
  message_retention_seconds = 86400
  receive_wait_time_seconds = 0
  visibility_timeout_seconds = 600
}


 